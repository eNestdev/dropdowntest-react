import { createStyles, Theme } from "@material-ui/core/styles";

export const styles = (theme: Theme) =>
  createStyles({
    root: { width: "500px", margin: "0 auto", padding: 8 },
    inputRoot: {
      '&&[class*="MuiOutlinedInput-root"]': {
        padding: 8,
      },
    },
  });
