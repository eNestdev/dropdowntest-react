import React, { Component, RefObject } from "react";
import AutoComplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import { observer } from "mobx-react";
import { action } from "mobx";
import { ISelectOption } from "../../Interfaces";
import { styles } from "./CustomAutoComplete.styles";
import { withStyles } from "@material-ui/core";

interface Props {
  classes?: { [key: string]: string };
  options: ISelectOption[];
  onInputChange: (value: ISelectOption) => void;
  placeHolder: string;
  value: ISelectOption;
}

@observer
class CustomAutoComplete extends Component<Props> {
  private textFieldRef: RefObject<HTMLInputElement> = React.createRef();

  public static defaultProps = {
    placeHolder: "Search",
  };

  // Pass data to parent component
  @action
  public onDropDownChange(value: ISelectOption): void {
    this.props.onInputChange(value);
  }

  render() {
    const { placeHolder, value, classes, options } = this.props;
    return (
      <AutoComplete
        fullWidth
        placeholder={placeHolder}
        value={value}
        options={options}
        getOptionSelected={(optionA, optionB) =>
          optionA.value === optionB.value
        }
        getOptionLabel={(option: ISelectOption) => option?.label || ""}
        onChange={(
          _,
          value: string | ISelectOption | (string | ISelectOption)[]
        ) => this.onDropDownChange(value as ISelectOption)}
        classes={{ root: classes.root, inputRoot: classes.inputField }}
        renderInput={(params) => (
          <TextField
            placeholder={placeHolder}
            inputRef={this.textFieldRef}
            {...params}
          />
        )}
      />
    );
  }
}

export default withStyles(styles)(CustomAutoComplete);
