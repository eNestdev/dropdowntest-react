import { action, observable } from "mobx";
import { observer } from "mobx-react";
import React, { Component, ReactNode } from "react";
import { CustomAutoComplete } from "../Component";
import { ISelectOption } from "../Interfaces";
import { TerritoryModel } from "../Models";

@observer
class Page1 extends Component {
  @observable private selectedValue: TerritoryModel = new TerritoryModel({
    id: 2,
    name: "Chandigarh",
  });

  private targets: TerritoryModel[] = [
    new TerritoryModel({ id: 1, name: "Andaman and Nicobar Islands" }),
    new TerritoryModel({ id: 2, name: "Chandigarh" }),
    new TerritoryModel({ id: 3, name: "Puducherry" }),
  ];

  @action
  private onInputChange(value: TerritoryModel): void {
    this.selectedValue = value;
  }

  render(): ReactNode {
    return (
      <>
        <CustomAutoComplete
          onInputChange={(selectedOption: ISelectOption) =>
            this.onInputChange(selectedOption as TerritoryModel)
          }
          value={this.selectedValue}
          placeHolder={"Select"}
          options={this.targets}
        />
        {this.selectedValue && (
          <span>{`Your selected value from drop down:  ${this.selectedValue.label}`}</span>
        )}
      </>
    );
  }
}

export default Page1;
