import { ISelectOption } from "../Interfaces";

export class TerritoryModel implements ISelectOption {
  id: number;
  name: string;

  constructor(data?: Partial<TerritoryModel>) {
    this.id = data.id;
    this.name = data.name;
  }

  static deserialize(apiData: TerritoryModel): TerritoryModel {
    if (!apiData) {
      return new TerritoryModel();
    }
    const data: Partial<TerritoryModel> = {
      id: apiData.id,
      name: apiData.name,
    };
    return new TerritoryModel(data);
  }

  // required in auto complete
  public get label(): string {
    return this.name;
  }

  public get value(): string | number {
    return this.id;
  }
}
