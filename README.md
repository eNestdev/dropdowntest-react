## How it works

1. Every Single Page Application build produces a consistent output with a single HTML Page

2. The HTML page - `index.html`, has references to style bundles and script bundles and a placeholder to render the client side application

3. Please run command yarn install to install packages.

4. To start app please run command yarn start.